.PHONY: lint
lint:
	#autopep8 --in-place --recursive --aggressive *.py
	python3.6 -mflake8 --config=setup.cfg --exclude='.git,venv,*migrations*,static/lib'
