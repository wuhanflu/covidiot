def bar(arg1,arg2):
    print("a")
    print("b")


def baz():
    print("x")
    print("y")
    break


def foo():
    print("1")
    print("2")
    print("3")
    print("4")
    print("5")
    print("6")
    print("7")
    print("8")
    print("9")
    print("10")

    print("11")
    print("12")
    print("13")
    print("14")
    print("15")

    bar('a',99)

    print("21")

    baz()

    for i in range(10):
        print(i)

    while True:
        print('a')
        if True:
            break
